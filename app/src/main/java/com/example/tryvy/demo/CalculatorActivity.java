package com.example.tryvy.demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CalculatorActivity extends AppCompatActivity {
    //Vars
    String currentTextView;
    String number1 = "";
    String number2 = "";
    Integer operatorSelected = 0;
    Boolean stepFinal = false;
    Float result = 0f;

    //Components
    TextView resultTextView;
    Button additionButton;
    Button subtractionButton;
    Button multiplicationButton;
    Button divisionButton;
    Button Number1Button;
    Button Number2Button;
    Button Number3Button;
    Button Number4Button;
    Button Number5Button;
    Button Number6Button;
    Button Number7Button;
    Button Number8Button;
    Button Number9Button;
    Button Number0Button;
    Button clearButton;
    Button calculateResultButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        setupToolbar();
        setupContentView();
        setupClickListener();
        resultTextView.setText(currentTextView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            Log.d("TRYVY","ON BACK");
            finish();
        }else if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Calculadora");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setupContentView(){
        resultTextView = (TextView) findViewById(R.id.resultTextview);
        additionButton = (Button) findViewById(R.id.btnSumar);
        subtractionButton = (Button) findViewById(R.id.btnRestar);
        multiplicationButton = (Button) findViewById(R.id.btnMultiplicar);
        divisionButton = (Button) findViewById(R.id.btnDividir);
        Number1Button = (Button) findViewById(R.id.button5);
        Number2Button = (Button) findViewById(R.id.button6);
        Number3Button = (Button) findViewById(R.id.button7);
        Number4Button = (Button) findViewById(R.id.button8);
        Number5Button = (Button) findViewById(R.id.button9);
        Number6Button = (Button) findViewById(R.id.button10);
        Number7Button = (Button) findViewById(R.id.button11);
        Number8Button = (Button) findViewById(R.id.button12);
        Number9Button = (Button) findViewById(R.id.button13);
        Number0Button = (Button) findViewById(R.id.button15);
        clearButton = (Button) findViewById(R.id.button16);
        calculateResultButton = (Button) findViewById(R.id.resultButton);
    }

    private void setupClickListener(){
        additionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( operatorSelected == 0 && number1!= "") {
                    operatorSelected = 1;
                    stepFinal = true;
                    currentTextView = "";
                    setupResult();
                }else{
                    Toast toast = Toast.makeText(CalculatorActivity.this,"Seleccione un número",Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
        subtractionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( operatorSelected == 0 && number1!= "") {
                    operatorSelected = 2;
                    stepFinal = true;
                    currentTextView = "";
                    setupResult();
                }else{
                    Toast toast = Toast.makeText(CalculatorActivity.this,"Seleccione un número",Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
        multiplicationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( operatorSelected == 0 && number1!= "") {
                    operatorSelected = 3;
                    stepFinal = true;
                    currentTextView = "";
                    setupResult();
                }else{
                    Toast toast = Toast.makeText(CalculatorActivity.this,"Seleccione un número",Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
        divisionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( operatorSelected == 0 && number1!= "") {
                    operatorSelected = 4;
                    stepFinal = true;
                    currentTextView = "";
                    setupResult();
                }else{
                    Toast toast = Toast.makeText(CalculatorActivity.this,"Seleccione un número",Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
        Number1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"1";
                    currentTextView = number2;
                }else{
                    number1 = number1+"1";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        Number2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"2";
                    currentTextView = number2;
                }else{
                    number1 = number1+"2";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        Number3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"3";
                    currentTextView = number2;
                }else{
                    number1 = number1+"3";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        Number4Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"4";
                    currentTextView = number2;
                }else{
                    number1 = number1+"4";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        Number5Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"5";
                    currentTextView = number2;
                }else{
                    number1 = number1+"5";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        Number6Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"6";
                    currentTextView = number2;
                }else{
                    number1 = number1+"6";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        Number7Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"7";
                    currentTextView = number2;
                }else{
                    number1 = number1+"7";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        Number8Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"8";
                    currentTextView = number2;
                }else{
                    number1 = number1+"8";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        Number9Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"9";
                    currentTextView = number2;
                }else{
                    number1 = number1+"9";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        Number0Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stepFinal){
                    number2 = number2+"0";
                    currentTextView = number2;
                }else{
                    number1 = number1+"0";
                    currentTextView = number1;
                }
                setupResult();
            }
        });
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearResults();
            }
        });
        calculateResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (operatorSelected > 0 && number2 != "") {
                    Log.d("TRYVY", "CALCULATE " + Float.parseFloat(number1) + " " + operatorSelected + " " + Float.parseFloat(number2));

                    result = 0f;
                    switch (operatorSelected) {
                        case 1:
                            Log.d("TRYVY", "+");
                            result = Float.parseFloat(number1) + Float.parseFloat(number2);
                            break;
                        case 2:
                            Log.d("TRYVY", "-");
                            result = Float.parseFloat(number1) - Float.parseFloat(number2);
                            break;
                        case 3:
                            Log.d("TRYVY", "*");
                            result = Float.parseFloat(number1) * Float.parseFloat(number2);
                            break;
                        case 4:
                            Log.d("TRYVY", "/");
                            result = Float.parseFloat(number1) / Float.parseFloat(number2);
                            break;
                    }

                    Log.d("TRYVY", "result " + result);
                    currentTextView = result.toString();
                    setupResult();
                    clearData();
                }else if (number2.equals("")){
                    Toast toast = Toast.makeText(CalculatorActivity.this,"Seleccione número 2",Toast.LENGTH_SHORT);
                    toast.show();
                }else{
                    Toast toast = Toast.makeText(CalculatorActivity.this,"Seleccione operador",Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
    }

    private void setupResult(){
        resultTextView.setText(currentTextView);
    }

    private void clearResults(){
        number1 = "";
        number2 = "";
        currentTextView = "";
        operatorSelected = 0;
        stepFinal = false;
        setupResult();
    }

    private void clearData(){
        operatorSelected = 0;
        number1 = "";
        number2 = "";
        stepFinal = false;
    }
}
